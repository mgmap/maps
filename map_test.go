package maps_test

import (
	"gitlab.com/mgmap/maps"
	"sort"
	"testing"
)

const (
	count = 100000
)

func TestHashMap(t *testing.T) {
	testMap(t, maps.NewHashMap(intHasherGood, intEquals))
	testMap(t, maps.NewHashMap(intHasherBad, intEquals))
	testMap(t, maps.NewHashMap(intHasherWorst, intEquals))
}

func assertValue(t *testing.T, m maps.Map, index int, expected interface{}) {
	value := m.Get(index)
	if value != expected {
		t.Fatalf("get fail. expected %v, but %v", expected, value)
	}
}

func assertValues(t *testing.T, m maps.Map, expectedVals ...int) {
	var vals []int
	for _, v := range m.Values() {
		vint := v.(int)
		vals = append(vals, vint)
	}
	if len(expectedVals) != len(vals) {
		t.Errorf("expected %v but got %v", expectedVals, vals)
		return
	}
	sort.Sort(sort.IntSlice(vals))
	sort.Sort(sort.IntSlice(expectedVals))
	for i := range vals {
		if vals[i] != expectedVals[i] {
			t.Errorf("expected %v but got %v", expectedVals, vals)
			return
		}
	}
}

func assertSize(t *testing.T, m maps.Map, expected int) {
	actual := m.Size()
	if actual != expected {
		t.Fatalf("size fail. expected %d, got %d", expected, actual)
	}
}

func testMap(t *testing.T, m maps.Map) {
	// new map should be empty
	assertSize(t, m, 0)
	assertValue(t, m, 1, nil)

	m.Put(1, 2)
	assertSize(t, m, 1)
	assertValue(t, m, 1, 2)
	assertValues(t, m, 2)

	m.Put(2, 4)
	assertSize(t, m, 2)
	assertValue(t, m, 2, 4)
	assertValues(t, m, 2, 4)

	// overwrite
	m.Put(1, 3)
	assertSize(t, m, 2)
	assertValue(t, m, 1, 3)
	assertValues(t, m, 3, 4)

	// delete
	m.Delete(1)
	assertSize(t, m, 1)
	assertValue(t, m, 1, nil)
	assertValues(t, m, 4)
	m.Delete(2)
	assertSize(t, m, 0)
	assertValue(t, m, 2, nil)
	assertValues(t, m)
}

func intHasherGood(x interface{}) uint32 {
	return uint32(x.(int))
}

func intHasherBad(x interface{}) uint32 {
	return uint32(x.(int) % 32)
}

func intHasherWorst(x interface{}) uint32 {
	return 1
}

func intEquals(x, y interface{}) bool {
	x1 := x.(int)
	y1 := y.(int)
	return x1 == y1
}

func BenchmarkTcbMapPut(b *testing.B) {
	for i := 0; i < b.N; i++ {
		m := maps.NewHashMap(intHasherGood, intEquals)
		putInts(b, m, count)
	}
}

func putInts(b *testing.B, m maps.Map, c int) {
	for i := 0; i < c; i++ {
		m.Put(i, i)
	}
	if m.Size() != count {
		b.Fatal("put fail.")
	}
}

func BenchmarkTcbMapGet(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var m maps.Map
		b.StopTimer()
		m = maps.NewHashMap(intHasherGood, intEquals)
		putInts(b, m, count)
		b.StartTimer()
		for i := 0; i < count; i++ {
			m.Get(i)
		}
	}
}

func BenchmarkTcbMapRemove(b *testing.B) {

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		m := maps.NewHashMap(intHasherGood, intEquals)
		putInts(b, m, count)
		b.StartTimer()
		for i := 0; i < count; i++ {
			m.Delete(i)
		}
		if m.Size() != 0 {
			b.Fatal("remove fail.")
		}
	}
}
